from django.test import TestCase
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.http import HttpRequest
from selenium import webdriver
import time
from .views import logIn, signUp, logOut
from .apps import Story9Config
from selenium.webdriver.chrome.options import Options
import os

# Create your tests here.

class UnitTestonStory9(TestCase):

    #LOGIN PAGE TEST   
    def test_check_login_page_have_form(self):
        request = HttpRequest()
        response = logIn(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<form', html_response)
    
    def test_signin_header(self):
        request = HttpRequest()
        response = logIn(request)
        html_response = response.content.decode('utf8')
        self.assertContains(response, "Sign in")

class FunctionalTestonStory9(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        self.chrome_options = Options()
        self.chrome_options.add_argument('--no-sandbox')
        self.chrome_options.add_argument('--headless')
        self.chrome_options.add_argument('--disable-gpu')
        self.chrome_path = './chromedriver'
        self.browser = webdriver.Chrome(options=self.chrome_options, executable_path=self.chrome_path)

    def tearDown(self):
        self.browser.quit()
        super().tearDown()

class TestApp(TestCase):
	def test_app(self):
		self.assertEqual(Story9Config.name, "story9")
