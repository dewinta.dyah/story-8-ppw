from django.urls import path
from .views import logIn, signUp, logOut

urlpatterns = [
    path('login', logIn, name='login'),
    path('signup/', signUp, name='signup'),
    path('logout/', logOut, name='logout'),
]
