from django.shortcuts import render
from django.http import JsonResponse
import json

# Create your views here.
def home(request):
    response = {}
    return render(request, "main/home.html", response)

def data(request):
    try:
        q = request.GET['q']
    except:
        q = "quilting"
    
    json_read = request.get('https://www.googleapis.com/books/v1/volumes?q=' + q).json()
    return JsonResponse(json_read)
